package ru.nstu.abramov.videorent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VideorentApplication {

	public static void main(String[] args) {
		SpringApplication.run(VideorentApplication.class, args);
	}
}