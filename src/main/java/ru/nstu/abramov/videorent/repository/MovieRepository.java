package ru.nstu.abramov.videorent.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.nstu.abramov.videorent.entity.Movie;

import java.util.List;

@Repository
public interface MovieRepository extends CrudRepository<Movie, Long> {

    Movie findByName(String name);
    List<Movie> findAllByGenre(String genre);
    List<Movie> findAll();
}