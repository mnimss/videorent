package ru.nstu.abramov.videorent.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.nstu.abramov.videorent.entity.Transaction;

import java.util.Collection;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Long> {

    Collection<Transaction> findAll();
}