package ru.nstu.abramov.videorent.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.nstu.abramov.videorent.entity.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {

    Customer findByLastName(String lastName);
    boolean existsAllByLastName(String lastName);
}