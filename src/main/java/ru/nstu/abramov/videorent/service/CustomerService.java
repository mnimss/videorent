package ru.nstu.abramov.videorent.service;

import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import ru.nstu.abramov.videorent.entity.Customer;
import ru.nstu.abramov.videorent.entity.Transaction;
import ru.nstu.abramov.videorent.repository.CustomerRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final TransactionService transactionService;

    public CustomerService(CustomerRepository customerRepository, TransactionService transactionService) {
        this.customerRepository = customerRepository;
        this.transactionService = transactionService;
    }

    public Customer create(Customer customer){

        customer.setLastName(customer.getLastName().trim());
        customer.setTransactions(new ArrayList<>());

        if (customerRepository.existsAllByLastName(customer.getLastName())){
            return null;
        }

        try {
            return customerRepository.save(customer);
        }
        catch (Exception e){
            return null;
        }
    }

    public Collection<Transaction> provideTransactions(String lastName, DateTime takeTime, String isCorrect){

        List<Transaction> result;
        try {
            result = customerRepository.findByLastName(lastName).getTransactions();
        }
        catch (Exception e){
            return null;
        }

        if (takeTime != null){
            result = transactionService.filterTakeTime(takeTime, result);
        }
        if (isCorrect != null && !isCorrect.equals("")){
            if (Boolean.valueOf(isCorrect)){
                result = transactionService.filterCorrectTransaction(Boolean.valueOf(isCorrect), result);
            }
        }

        return result;
    }

    public Customer provide(String lastName){

        try{
            return customerRepository.findByLastName(lastName);
        }
        catch (Exception e){
            return null;
        }
    }
}