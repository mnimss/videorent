package ru.nstu.abramov.videorent.service;

import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import ru.nstu.abramov.videorent.entity.Transaction;
import ru.nstu.abramov.videorent.repository.TransactionRepository;

import java.util.*;

@Service
public class TransactionService {

    private final TransactionRepository transactionRepository;

    public TransactionService(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public Transaction create(Transaction transaction){

        return transactionRepository.save(transaction);
    }

    public Transaction close(Transaction transaction){

        Optional<Transaction> result = transactionRepository.findById(transaction.getId());

        if (result.isPresent()){
            result.get().setRealResetTime(transaction.getRealResetTime());
            transactionRepository.save(result.get());

            return result.get();
        }
        else {
            return null;
        }
    }

    public Optional<Integer> getProfit(String from, String until){

        DateTime start = DateTime.parse(from);
        DateTime end = DateTime.parse(until);

        return getAllTransactionsByTime(start, end)
                .stream()
                .map(o -> o.getMovie().getPrice())
                .reduce(Integer::sum);
    }

    private Collection<Transaction> getAllTransactionsByTime(DateTime start, DateTime end) {

        if (end == null){
            end = DateTime.now();
        }

        Collection<Transaction> result = new ArrayList<>();

        DateTime finalEnd = end;
        transactionRepository.findAll().forEach(o -> {

            if (finalEnd.isAfter(Transaction.convert(o.getTakeTime())) && Transaction.convert(o.getTakeTime()).isAfter(start)){
                result.add(o);
            }
        });

        return result;
    }

    List<Transaction> filterTakeTime(DateTime time, List<Transaction> transactions){

        transactions
                .removeIf(transaction ->
                        Transaction.convert(transaction.getTakeTime()).getDayOfYear() != time.getDayOfYear());

        return transactions;
    }

    List<Transaction> filterCorrectTransaction(boolean isCorrect, List<Transaction> transactions){

        Iterator<Transaction> iterator = transactions.iterator();

        while (iterator.hasNext()){

            Transaction o = iterator.next();

            if (o.getRealResetTime() == null){
                continue;
            }

            if (Transaction.convert(o.getResetTime()).isAfter(Transaction.convert(o.getRealResetTime())) != isCorrect){

                iterator.remove();
            }
        }

        return transactions;
    }

    public List<Transaction> provideAllTransactions(DateTime takeTime, String isCorrect){

        List<Transaction> result;
        try {
            result = new ArrayList<>(transactionRepository.findAll());
        }
        catch (Exception e){
            return null;
        }

        if (takeTime != null){
            result = filterTakeTime(takeTime, result);
        }
        if (!isCorrect.equals("")){
            if (Boolean.valueOf(isCorrect)){
                result = filterCorrectTransaction(Boolean.valueOf(isCorrect), result);
            }
        }

        return result;
    }
}