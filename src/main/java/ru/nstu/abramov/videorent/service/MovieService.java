package ru.nstu.abramov.videorent.service;

import org.springframework.stereotype.Service;
import ru.nstu.abramov.videorent.entity.Movie;
import ru.nstu.abramov.videorent.repository.MovieRepository;

import java.util.*;

@Service
public class MovieService {

    private final MovieRepository movieRepository;

    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public Movie provide(String name){

        try{
            return movieRepository.findByName(name);
        }
        catch (Exception e){
            return null;
        }
    }

    public List<Movie> provideAllMovies(String genre){

        try {
            if (genre != null && !genre.equals("")){
                return movieRepository.findAllByGenre(genre);
            }
            else {
                return movieRepository.findAll();
            }
        }
        catch (Exception e){
            return null;
        }
    }

    public Movie create(Movie movie){

        movie.setGenre(movie.getGenre().trim());
        movie.setName(movie.getName().trim());
        movie.setTransactions(new ArrayList<>());

        try {
            return movieRepository.save(movie);
        }
        catch (Exception e){
            return null;
        }
    }
}