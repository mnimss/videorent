package ru.nstu.abramov.videorent.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "CUSTOMER")
public class Customer implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    @Column(name = "lastName", nullable = false)
    private String lastName;

    @OneToMany(mappedBy = "customer", fetch = FetchType.EAGER)
    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "customerInTransaction")
    @Column(name = "transaction")
    @JsonIgnore
    private List<Transaction> transactions;

    public Customer() {
    }

    public Customer(String lastName, List<Transaction> transactions) {
        this.lastName = lastName;
        this.transactions = transactions;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Customer)) return false;
        Customer customer = (Customer) o;
        return getLastName().equals(customer.getLastName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLastName());
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}