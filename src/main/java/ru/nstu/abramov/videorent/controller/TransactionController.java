package ru.nstu.abramov.videorent.controller;

import org.joda.time.DateTime;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nstu.abramov.videorent.entity.Transaction;
import ru.nstu.abramov.videorent.service.TransactionService;

import java.util.*;

@RestController
public class TransactionController {

    private static final String TRANSACTION_PATH = PathUtil.TRANSACTION_PREFIX;
    private final TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @PostMapping(TRANSACTION_PATH)
    public @ResponseBody
    ResponseEntity<Transaction> create(@RequestBody Transaction transaction) {

        if (transaction == null
        || transaction.getTakeTime() == null
        || transaction.getMovie() == null
        || transaction.getResetTime() == null
        || transaction.getCustomer() == null){

            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(transactionService.create(transaction));
    }

    @PostMapping(TRANSACTION_PATH + "/close")
    public @ResponseBody
    ResponseEntity<Transaction> close(@RequestBody Transaction transaction) {

        if (transaction == null){
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(transactionService.close(transaction));
    }

    @GetMapping(TRANSACTION_PATH + "/profit")
    public @ResponseBody
    ResponseEntity<Integer> getProfit(@RequestParam Map<String, String> times) {

        if (times == null
                || times.get("start") == null){

            return ResponseEntity.badRequest().build();
        }

        Optional<Integer> result = transactionService.getProfit(times.get("start"), times.get("end"));
        if (result.isPresent()){
            return ResponseEntity.ok(result.get());
        }
        else {
            return ResponseEntity.ok(0);
        }
    }

    @GetMapping(TRANSACTION_PATH)
    public @ResponseBody
    ResponseEntity<List<Transaction>> provideAllTransactions(@RequestParam Map<String, String> filters) {

        DateTime takeTime;
        if (filters.get("takeTime").equals("")){
            takeTime = null;
        }
        else {
            takeTime = DateTime.parse(filters.get("takeTime"));
        }

        List<Transaction> result = transactionService.provideAllTransactions(takeTime, filters.get("isCorrect"));
        if (result == null){
            return ResponseEntity.badRequest().build();
        }
        else {
            return ResponseEntity.ok(result);
        }
    }
}