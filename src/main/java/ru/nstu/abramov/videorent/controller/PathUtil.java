package ru.nstu.abramov.videorent.controller;

class PathUtil {

    static final String CUSTOMER_PREFIX = "/customers";
    static final String MOVIE_PREFIX = "/movies";
    static final String TRANSACTION_PREFIX = "/transactions";
}