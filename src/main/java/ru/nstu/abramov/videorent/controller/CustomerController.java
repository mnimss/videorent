package ru.nstu.abramov.videorent.controller;

import org.joda.time.DateTime;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nstu.abramov.videorent.entity.Transaction;
import ru.nstu.abramov.videorent.entity.Customer;
import ru.nstu.abramov.videorent.service.CustomerService;

import java.util.Collection;
import java.util.Map;

@RestController
public class CustomerController {

    private static final String CUSTOMER_PATH = PathUtil.CUSTOMER_PREFIX;
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping(CUSTOMER_PATH + "/lastName/{lastName}/transactions")
    public @ResponseBody
    ResponseEntity<Collection<Transaction>> provideTransactions(@PathVariable(name = "lastName") String lastName,
                                                                @RequestParam Map<String, String> filters) {

        DateTime takeTime;
        if (filters.get("takeTime").equals("")){
            takeTime = null;
        }
        else {
            takeTime = DateTime.parse(filters.get("takeTime"));
        }

        if (lastName == null || lastName.trim().equals("")){
            return ResponseEntity.badRequest().build();
        }

        Collection<Transaction> result = customerService.provideTransactions(lastName, takeTime, filters.get("isCorrect"));

        if (result == null){
            return ResponseEntity.badRequest().build();
        }
        else {
            return ResponseEntity.ok(result);
        }
    }

    @PostMapping(CUSTOMER_PATH)
    public @ResponseBody
    ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) {

        if (customer == null
        || customer.getLastName() == null
        || customer.getLastName().trim().equals("")){

            return ResponseEntity.badRequest().build();
        }

        Customer result = customerService.create(customer);

        if (result == null){
            return ResponseEntity.badRequest().build();
        }
        else {
            return ResponseEntity.ok(result);
        }
    }

    @GetMapping(CUSTOMER_PATH + "/lastName/{lastName}")
    public @ResponseBody
    ResponseEntity<Customer> provideCustomer(@PathVariable("lastName") String lastName) {

        if (lastName == null || lastName.trim().equals("")){
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(customerService.provide(lastName));
    }
}