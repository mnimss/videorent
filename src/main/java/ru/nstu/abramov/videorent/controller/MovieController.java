package ru.nstu.abramov.videorent.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nstu.abramov.videorent.entity.Movie;
import ru.nstu.abramov.videorent.service.MovieService;

import java.util.List;
import java.util.Map;

@RestController
public class MovieController {

    private static final String MOVIE_PATH = PathUtil.MOVIE_PREFIX;
    private final MovieService movieService;

    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @PostMapping(MOVIE_PATH)
    public @ResponseBody
    ResponseEntity<Movie> create(@RequestBody Movie movie) {

        if (movie == null
        || movie.getGenre().trim().equals("")
        || movie.getGenre() == null
        || movie.getPrice() < 0
        || movie.getName().trim().equals("")
        || movie.getName() == null){

            return ResponseEntity.badRequest().build();
        }

        Movie result = movieService.create(movie);

        if (result == null){
            return ResponseEntity.badRequest().build();
        }
        else {
            return ResponseEntity.ok(result);
        }
    }

    @GetMapping(MOVIE_PATH + "/name/{name}")
    public @ResponseBody
    ResponseEntity<Movie> provideMovie(@PathVariable("name") String name) {

        if (name == null || name.trim().equals("")){
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(movieService.provide(name));
    }

    @GetMapping(MOVIE_PATH)
    public @ResponseBody
    ResponseEntity<List<Movie>> provideAllMovies(@RequestParam Map<String, String> filters) {

        return ResponseEntity.ok(movieService.provideAllMovies(filters.get("genre")));
    }
}