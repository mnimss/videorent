package ru.nstu.abramov.videorent.repository;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.nstu.abramov.videorent.entity.Customer;
import ru.nstu.abramov.videorent.entity.Movie;
import ru.nstu.abramov.videorent.entity.Transaction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionRepositoryTest {

    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private CustomerRepository customerRepository;

    private Customer customer;
    private Movie[] movies = new Movie[2];

    @After
    public void tearDown() throws Exception {
        transactionRepository.deleteAll();
        customerRepository.deleteAll();
        movieRepository.deleteAll();
    }

    @Before
    public void setUp() throws Exception {

        Customer customer = new Customer();
        customer.setLastName("Абрамов");
        this.customer = customerRepository.save(customer);

        Movie movie = new Movie();
        movie.setName("Один дома");
        movie.setGenre("Комедия");
        movie.setPrice(199);
        this.movies[0] = movieRepository.save(movie);

        Movie movie2 = new Movie();
        movie2.setName("Другой фильм");
        movie2.setGenre("не комедия");
        movie2.setPrice(991);
        this.movies[1] = movieRepository.save(movie2);
    }

    @Test
    public void save() {

        Transaction transaction = new Transaction();
        transaction.setCustomer(customer);
        transaction.setMovie(movies[0]);
        transaction.setTakeTime("2010-06-30T01:20");
        transaction.setResetTime("2010-07-03T01:20");

        Transaction actual = transactionRepository.save(transaction);

        Assert.assertEquals(transaction, actual);
    }

    @Test
    public void findAll() {

        Transaction transaction = new Transaction();
        transaction.setCustomer(customer);
        transaction.setMovie(movies[0]);
        transaction.setTakeTime("2010-06-30T01:20");
        transaction.setResetTime("2010-07-03T01:20");
        transactionRepository.save(transaction);

        Transaction transaction2 = new Transaction();
        transaction2.setCustomer(customer);
        transaction2.setMovie(movies[1]);
        transaction2.setTakeTime("2012-06-30T01:20");
        transaction2.setResetTime("2012-07-03T01:20");
        transactionRepository.save(transaction2);

        List<Transaction> expected = new ArrayList<>();
        expected.add(transaction);
        expected.add(transaction2);

        Collection<Transaction> temp = transactionRepository.findAll();
        List<Transaction> actual = new ArrayList<>(temp);

        Assert.assertEquals(expected.size(), actual.size());

        Assert.assertEquals(expected.get(0), actual.get(0));
        Assert.assertEquals(expected.get(1), actual.get(1));
    }

    @Test
    public void findById() {

        Transaction transaction = new Transaction();
        transaction.setCustomer(customer);
        transaction.setMovie(movies[0]);
        transaction.setTakeTime("2010-06-30T01:20");
        transaction.setResetTime("2010-07-03T01:20");
        Transaction expected = transactionRepository.save(transaction);

        Transaction transaction2 = new Transaction();
        transaction2.setCustomer(customer);
        transaction2.setMovie(movies[1]);
        transaction2.setTakeTime("2012-06-30T01:20");
        transaction2.setResetTime("2012-07-03T01:20");
        transactionRepository.save(transaction2);

        Optional<Transaction> actual = transactionRepository.findById(expected.getId());

        Assert.assertEquals(expected.getId(), actual.get().getId());
    }
}