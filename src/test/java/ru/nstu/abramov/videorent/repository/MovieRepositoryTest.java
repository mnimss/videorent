package ru.nstu.abramov.videorent.repository;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.nstu.abramov.videorent.entity.Movie;

import java.util.Collection;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MovieRepositoryTest {

    @Autowired
    private MovieRepository movieRepository;

    @After
    public void tearDown() throws Exception{
        movieRepository.deleteAll();
    }

    @Test
    public void save() {

        Movie movie = new Movie();
        movie.setName("Один дома");
        movie.setGenre("Комедия");
        movie.setPrice(199);

        Movie result = movieRepository.save(movie);

        Assert.assertEquals(movie, result);
    }

    @Test
    public void findByName() {

        Movie movie = new Movie();
        movie.setName("Один дома");
        movie.setGenre("Комедия");
        movie.setPrice(199);

        movieRepository.save(movie);

        String name = "Один дома";

        Movie actual = movieRepository.findByName(name);

        Assert.assertEquals(movie.getId(), actual.getId());
    }

    @Test
    public void findAllByGenre() {

        Movie movie = new Movie();
        movie.setName("Один дома");
        movie.setGenre("Комедия");
        movie.setPrice(199);
        movieRepository.save(movie);

        Movie movie2 = new Movie();
        movie2.setName("Другой фильм");
        movie2.setGenre("не комедия");
        movie2.setPrice(991);
        movieRepository.save(movie2);

        String genre = "Комедия";

        Collection<Movie> movies = movieRepository.findAllByGenre(genre);
        Optional<Movie> actual = movies.stream().findFirst();

        Assert.assertEquals(movies.size(), 1);
        Assert.assertEquals(actual.get().getId(), movie.getId());
    }
}