package ru.nstu.abramov.videorent.repository;

import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.nstu.abramov.videorent.entity.Customer;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerRepositoryTest {

    @Autowired
    private CustomerRepository customerRepository;

    @After
    public void tearDown() {

        customerRepository.deleteAll();
    }

    @Test
    public void save() {

        Customer customer = new Customer();
        customer.setLastName("Абрамов");

        Customer result = customerRepository.save(customer);

        Assert.assertEquals(customer, result);
    }

    @Test
    public void findByLastName() {

        String lastName = "Абрамов";

        Customer expected = new Customer();
        expected.setLastName("Абрамов");

        customerRepository.save(expected);

        Customer actual = customerRepository.findByLastName(lastName);

        Assert.assertEquals(expected.getId(), actual.getId());
    }
}